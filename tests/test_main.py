import unittest

from src.main import alternating_characters

class TestAlternatingChars(unittest.TestCase):

    def test_all_as(self):
        changes = alternating_characters('AAA')
        self.assertEqual(changes, 2)

    def test_all_bs(self):
        changes = alternating_characters('BBBB')
        self.assertEqual(changes, 3)

    def test_alternating_chars(self):
        changes = alternating_characters('BABABABA')
        self.assertEqual(changes, 0)

    def test_single_char(self):
        changes = alternating_characters('A')
        self.assertEqual(changes, 0)

    def test_mixed_alterating(self):
        changes = alternating_characters('AABBAAABBB')
        self.assertEqual(changes, 6)

