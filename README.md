# Live Coder Demo Project

This is an example project for Live Coder.

Checkout the [Server](https://pypi.org/project/live-coder/) and [VSCode extension](https://marketplace.visualstudio.com/items?itemName=fraser.live-coder#review-details).

## How to use?

1. Install the server and extension.
2. Download this project.
3. Open it in VSCode.
4. Start the server by entering `live-coder` into a terminal.
5. In VSCode enter `shift + cmd + p` and type `live coder`, select "Live Coder: Show Live Values".
6. Select the test class `TestAlternatingChars`.
7. You should see some Live Values!

If you have any issues contact me: `frassyg@gmail.com`
