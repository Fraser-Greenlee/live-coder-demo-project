

def alternating_characters(s):
    alternating_string = ''
    deletion_count = 0
    last_char = None
    for char in s:
        if char == last_char:
            deletion_count += 1
        else:
            last_char = char
            alternating_string += char
    print(alternating_string)
    return deletion_count
